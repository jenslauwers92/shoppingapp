package be.vdab.Winkelen;


import java.nio.channels.ScatteringByteChannel;
import java.time.LocalDateTime;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

public class Person implements IPerson {
    ShoppingCenter sc = new ShoppingCenter("Wijnegem", "Wijnegem Shoppingcenter", 0);


    Scanner keyboard = new Scanner(System.in);
    String name;
    Gender gender;
    ICart cart;
    int age;

    public Person() {
        cart = new SmallCart();
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }


    public void setAge(int age) {
        this.age = age;
    }


    Person(String name, int age, Gender gender, ICart cart) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.cart = new SmallCart();


    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public ICart getCart() {
        return cart;
    }

    @Override
    public void walkToPlace(String placeName) {
        String[] s = {"It's a windy day.", "It's snowing how magical!", "It's a warm sunny day.", "It's a sober rainy day."};

        Random ran = new Random();
        String s_ran = s[ran.nextInt(s.length)];


        System.out.println("Enter your name");
        name = keyboard.nextLine();
        while (true) {
            try {

                System.out.println("Enter your age");
                age = keyboard.nextInt();
                break;

            } catch (InputMismatchException exc) {

                System.out.println("Enter a proper value");

            } finally {
                keyboard.nextLine();
            }
        }
        while (true) {
            try {

                System.out.println("Enter your gender");
                gender = Gender.valueOf(keyboard.next().toUpperCase());
                break;
            } catch (IllegalArgumentException ie) {
                System.out.println("Enter m or f");
            } finally {
                keyboard.nextLine();
            }


            System.out.println("your name: " + name);
            System.out.println("Your age: " + age);
            System.out.println("Your gender: " + gender.sex);
            LocalDateTime ldt = LocalDateTime.now();
            System.out.println("Today, " + ldt);
            System.out.println(s_ran);
            System.out.println(name + " looks to the " + placeName);
            System.out.println("without thinking he decides to enter.");


        }
    }
}
