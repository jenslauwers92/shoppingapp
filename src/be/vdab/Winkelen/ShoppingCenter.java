package be.vdab.Winkelen;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ShoppingCenter implements IStore {

    private String name;
    private String description;

    private int choice = 0;
    IStore[] stores = new IStore[3];
    IStore bar = new Bar("bar", "The drunken shopper", "water ","coffee " ,"beer " );
    IStore cs = new ClothingStore("Clothingstore","Fashion store", "Red shirt", "Leather jacket", "Scarf");
    IStore comps = new ComputerStore("Computerstore", "The computergeek","Laptop ","Smartphone ", "hdmicable" );


    public ShoppingCenter(String name, String description, int choice){
        this.name = name;
        this.description = description;
        this.choice = choice;
    }

    public int getChoice() {
        Scanner keyboard = new Scanner(System.in);
        try {

            choice = keyboard.nextInt();
        }catch(InputMismatchException ime){
            System.out.println("Enter a valid value");
            getChoice();
        }

        return choice;
    }
    private void handleChoice(IPerson person, int choice){
        if(choice >=0 && choice <=2){
            //person walks store
            System.out.println(person.getName() + " walks to the " +stores[choice].getName());


                stores[choice].enterStore(person);


        }else if(choice == -1){
            System.out.println(person.getName()+ " walks to the exit of the " + getDescription());
        }else{
            System.out.println("enter valid choice");
            showMenu();
        }


    }
    private void showMenu(){
        stores[0] = bar;
        stores[1] = cs;
        stores[2] = comps;
        System.out.println("Store list: " + stores.length);
        for(int i=0; i<stores.length;i++){
            IStore store = stores[i];
            System.out.println(i+"."+" "+ store.getName() +" "+ store.getDescription());
        }
        System.out.println("-1. exit store");




    }


    @Override
    public void enterStore(IPerson person) {
do {


    showMenu();
    getChoice();
    System.out.println("Your choice " + choice);
    handleChoice(person, choice);

}while(choice != -1);
       // showMenu();
        //System.out.println("You chose: " + choice);
        //handleChoice(person, choice);

    }
    //    Bar bar = new Bar();
      //  ClothingStore cs = new ClothingStore();
        //ComputerStore comps = new ComputerStore();

       // int choice;
        //do {
          //  System.out.println(person.getName() + " is in the shopping center");
            //System.out.println("1. Bar ");
           // System.out.println("2. Fashion store");
            //System.out.println("3. Computer store");
            //System.out.println("0. exit the shopping center");
            //System.out.print("Your choice: ");
            //choice = keyboard.nextInt();
            //switch (choice) {
              //  case 1:
                //    bar.bar();
                  //  break;
             //   case 2:
               //     cs.clothingStore();
                 //   break;
        //        case 3:
          //          comps.computerStore();
            //        break;
              //  case 0:
                //    System.out.println("See you next time, " + person.getName());
                  //  break;
             //   default:
               //     System.err.println("Make a proper choice");
           // }
        //} while (choice != 0);




    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
