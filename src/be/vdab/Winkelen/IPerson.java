package be.vdab.Winkelen;


public interface IPerson {


    String getName();
    int getAge();
    Gender getGender();
    ICart getCart();

    public abstract void walkToPlace(String placeName);






}

