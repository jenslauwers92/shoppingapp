package be.vdab.Winkelen;

public interface IStore {

    void enterStore(IPerson person);
    String getDescription();
    String getName();
    public int getChoice();


}
