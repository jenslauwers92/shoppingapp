package be.vdab.Winkelen;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class ItemStore implements IStore {

    private int choice;
    protected String[] items;
    private String name;
    private String description;


    public ItemStore(String name, String description, String... items) {

        this.name = name;
        this.description = description;
        this.items = items;

    }


    protected void handleChoice(IPerson person, int choice) {

        System.out.println(choice);
        if (choice >= 0 && choice < items.length) {

            System.out.println(person.getName() + " buys a " + items[choice]);
            person.getCart().enterItem(items[choice]);


        } else if (choice == -1) {
            System.out.println(person.getName()+ " walks out of the " + getName());
        } else {

            System.out.println("enter valid choice");

        }
    }


    public void showMenu() {
        System.out.println("Items for sale: " + items.length);
        for (int i = 0; i < items.length; i++) {
            System.out.println(i + "." + " " + items[i]);

        }
        System.out.println("-1. to exit the store ");


    }

    public int getChoice() {
        Scanner keyboard = new Scanner(System.in);
        try {

            choice = keyboard.nextInt();
        } catch (InputMismatchException ime) {
            System.out.println("Enter a valid value");
            getChoice();

        } finally {
            keyboard.nextLine();
        }

        return choice;
    }


    @Override
    public void enterStore(IPerson person) {


        do {


            showMenu();
            int choice = getChoice();
            handleChoice(person, choice);
        } while (choice != -1);

        }


    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getName() {
        return name;
    }
}
