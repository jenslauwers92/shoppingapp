package be.vdab.Winkelen;

public class SmallCart implements ICart{
 final static int MAX_ITEMS = 10;
 private boolean isFull;
 private int itemCount;
 String[]  itemList = new String[MAX_ITEMS];



 public String[] getItemList() {
  return this.itemList;
 }

 public void setItemList(String[] itemList) {
  this.itemList = itemList;
 }

 public String toString() {
  return null;
 }

 @Override
 public int getMaxItems() {
  return MAX_ITEMS;
 }

 @Override
 public boolean isFull() {
  if (itemCount == MAX_ITEMS) {

   return true;
  }
  return false;
 }

 @Override
 public int getItemCount() {
  return itemCount;
 }

 @Override
 public void enterItem(String itemName) {

 if(!isFull()){
  itemList[itemCount]= itemName;
  itemCount++;
 }else{
  System.out.println("Your cart is full");
 }


 }
}
