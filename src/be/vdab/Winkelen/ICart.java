package be.vdab.Winkelen;

public interface ICart {

int getMaxItems();
boolean isFull();
int getItemCount();
void enterItem(String ItemName);
    public String[] getItemList();


}
