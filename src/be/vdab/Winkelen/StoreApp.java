package be.vdab.Winkelen;

import java.util.Random;

public class StoreApp {

    public static void main(String[] args) {

IStore sc = new ShoppingCenter("Wijnegem", "Wijnegem Shopping center", 0);


        IPerson person = new Person();
       startSimulation(person,sc);


    }
    public static void startSimulation(IPerson person, IStore store){

        person.walkToPlace("Shoppingcenter");
        store.enterStore(person);
        System.out.println("#items in your cart: " + person.getCart().getItemCount());
        for (int i = 0;i<person.getCart().getItemList().length;i++) {
            if (person.getCart().getItemList()[i] != null) {
                System.out.println(person.getCart().getItemList()[i]);
            }
        }
    }
    public StoreApp(){

    }
}
