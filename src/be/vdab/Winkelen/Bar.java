package be.vdab.Winkelen;

import java.util.Scanner;

public class Bar extends ItemStore {


    public Bar(String name, String description, String... items) {
        super(name, description, items);


    }

    @Override
    protected void handleChoice(IPerson person, int choice) {

        System.out.println(choice);
        if (choice >= 0 && choice < items.length ) {

            if (choice==2 && person.getAge() < 18) {
                System.out.println("Too young for beer");
            } else {
                System.out.println(person.getName() + " buys a " + items[choice]);
                person.getCart().enterItem(items[choice]);
            }


        } else if (choice == -1) {
            System.out.println(person.getName() + " walks out of the " + getName());
        } else {

            System.out.println("enter valid choice");

        }
    }
}

